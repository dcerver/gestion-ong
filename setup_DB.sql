DROP DATABASE IF EXISTS gestionOng;
CREATE DATABASE gestionOng;

DROP USER IF EXISTS 'administrador'@'localhost';
CREATE USER 'administrador'@'localhost' IDENTIFIED BY 'contraseña';
GRANT ALL PRIVILEGES ON * . * TO 'administrador'@'localhost';
GRANT ALL ON gestionOng.* TO 'administrador'@'localhost' IDENTIFIED BY 'contraseña' WITH GRANT OPTION;
FLUSH PRIVILEGES;

USE gestionOng;

DROP TABLE IF EXISTS proyectos;
CREATE TABLE proyectos (
  id int(20) NOT NULL,
  pais varchar(255) NOT NULL,
  localizacion varchar(255) NOT NULL,
  linea varchar(255) NOT NULL,
  sublinea varchar(255) NOT NULL,
  fechaInicio varchar(255) NOT NULL,
  fechaFin varchar(255) NOT NULL,
  financiacion  varchar(255) NOT NULL,
  acciones varchar(255) NOT NULL,
  PRIMARY KEY (id)
);
