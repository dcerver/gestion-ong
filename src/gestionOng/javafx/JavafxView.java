package gestionOng.javafx;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import gestionOng.logic.Proyecto;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class JavafxView extends Application {

	private TextField pais, localizacion, linea, sublinea, financiacion, acciones;
	private DatePicker fechaInicio, fechaFin;
	private Button botonGuardar;
	
	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Introduce un Nuevo Proyecto");
		
		Group root = new Group();
		Scene scene = new Scene(root, 400, 300);
		
		pais = new TextField();
		pais.setTooltip(new Tooltip("Introduce el pais"));
		pais.setFont(Font.font("SanSerif", 15));
		pais.setPromptText("Pais");
		pais.setMaxWidth(200);
		
		localizacion = new TextField();
		localizacion.setTooltip(new Tooltip("Introduce la localizacion"));
		localizacion.setFont(Font.font("SanSerif", 15));
		localizacion.setPromptText("Localizacion");
		localizacion.setMaxWidth(200);
		
		linea = new TextField();
		linea.setTooltip(new Tooltip("Introduce la linea de accion"));
		linea.setFont(Font.font("SanSerif", 15));
		linea.setPromptText("Linea de accion");
		linea.setMaxWidth(200);
		
		sublinea = new TextField();
		sublinea.setTooltip(new Tooltip("Introduce la sublinea de accion"));
		sublinea.setFont(Font.font("SanSerif", 15));
		sublinea.setPromptText("Sublinea de accion");
		sublinea.setMaxWidth(200);
		
		financiacion = new TextField();
		financiacion.setTooltip(new Tooltip("Introduce la financiacion"));
		financiacion.setFont(Font.font("SanSerif", 15));
		financiacion.setPromptText("Financiacion");
		financiacion.setMaxWidth(200);
		
		acciones = new TextField();
		acciones.setTooltip(new Tooltip("Introduce las acciones"));
		acciones.setFont(Font.font("SanSerif", 15));
		acciones.setPromptText("Acciones");
		acciones.setMaxWidth(200);
		
		fechaInicio = new DatePicker();
		fechaInicio.setTooltip(new Tooltip("Introduce la fecha de inicio"));		
		fechaInicio.setPromptText("Fecha de inicio");
		fechaInicio.setMaxWidth(200);
		fechaInicio.setStyle("-fx-font-size:15");
		
		fechaFin = new DatePicker();
		fechaFin.setTooltip(new Tooltip("Introduce la fecha de finalizacion"));		
		fechaFin.setPromptText("Fecha de finalizacion");
		fechaFin.setMaxWidth(200);
		fechaFin.setStyle("-fx-font-size:15");
		
        // Configuracion de Hibernate
		Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
		SessionFactory sf = cfg.buildSessionFactory();
					
		Proyecto proyecto = new Proyecto();
		
		botonGuardar = new Button("Guardar");
		botonGuardar.setTooltip(new Tooltip("Guarda los datos."));
		botonGuardar.setFont(Font.font("SanSerif", 15));
		botonGuardar.setOnAction(e ->{
			proyecto.setPais(pais.getText());
			proyecto.setLocalizacion(localizacion.getText());
//			proyecto.setLinea(linea.getText());
//			proyecto.setSubLinea(sublinea.getText());
//			proyecto.setFinanciacion(financiacion.getText());
			proyecto.setAcciones(acciones.getText());
//			proyecto.setFechaInicio(fechaInicio.getEditor().getText());
//			proyecto.setFechaFin(fechaFin.getEditor().getText());

			Session session = sf.openSession();
			session.beginTransaction();
			session.save(proyecto);
			session.getTransaction().commit();
			session.close();
			
			clearFields();			
		});
		
		VBox vbox = new VBox(10);
		vbox.getChildren().addAll(pais, localizacion, linea, sublinea, fechaInicio, fechaFin, financiacion, acciones, botonGuardar);
		vbox.setPadding(new Insets(10));		
		root.getChildren().add(vbox);
		
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	private void clearFields() {
		pais.clear();
		localizacion.clear();
		linea.clear();
		sublinea.clear();
		financiacion.clear();
		acciones.clear();
		fechaInicio.getEditor().setText(null);	
		fechaInicio.setValue(null);
		fechaFin.getEditor().setText(null);	
		fechaFin.setValue(null);
	}

	public static void main(String[] args) {
		launch(args);
	}
}


