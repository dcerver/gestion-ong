package gestionOng.logic;
/**
 * @author Software Solutions
 * Financiador del proyecto
 */
public class Financiador {
	private Proyecto proyecto;
	private String tipo;
	
	/**
	 * @param proyecto
	 * @param tipo
	 */
	public Financiador(Proyecto proyecto, String tipo) {
		super();
		this.proyecto = proyecto;
		this.tipo = tipo;
	}
	/**
	 * @return the proyecto
	 */
	public Proyecto getProyecto() {
		return proyecto;
	}
	/**
	 * @param proyecto the proyecto to set
	 */
	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}
