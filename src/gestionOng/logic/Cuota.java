package gestionOng.logic;
import java.util.ArrayList;

/**
 * @author Software Solutions
 * Cuota que pagan los socios a la organizacion.
 */
public class Cuota extends Ente {
	private SedeCentral sede;
	private ArrayList<Socio> socios;
	private String tipo;
	private float importe;
	
	/**
	 * @param id
	 * @param nombre
	 * @param sede
	 * @param socios
	 * @param tipo
	 * @param importe
	 */
	public Cuota(int id, String nombre, SedeCentral sede, ArrayList<Socio> socios, String tipo, float importe) {
		super(id, nombre);
		this.sede = sede;
		this.socios = socios;
		this.tipo = tipo;
		this.importe = importe;
	}
	/**
	 * @return the sede
	 */
	public SedeCentral getSede() {
		return sede;
	}
	/**
	 * @param sede the sede to set
	 */
	public void setSede(SedeCentral sede) {
		this.sede = sede;
	}
	/**
	 * @return the socios
	 */
	public ArrayList<Socio> getSocios() {
		return socios;
	}
	/**
	 * @param socios the socios to set
	 */
	public void setSocios(ArrayList<Socio> socios) {
		this.socios = socios;
	}
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return the importe
	 */
	public float getImporte() {
		return importe;
	}
	/**
	 * @param importe the importe to set
	 */
	public void setImporte(float importe) {
		this.importe = importe;
	}
	
}
