package gestionOng.logic;
import java.util.ArrayList;

/**
 * @author Software Solutions
 * Lineas de Accion de la organizacion, en distintos paises.
 */
public class LineaAccion extends Ente {
	private SedeCentral sede;
	private ArrayList<SubLineaAccion> sublineas;
	
	/**
	 * @param id
	 * @param nombre
	 * @param sede
	 * @param sublineas
	 */
	public LineaAccion(int id, String nombre, SedeCentral sede, ArrayList<SubLineaAccion> sublineas) {
		super(id, nombre);
		this.sede = sede;
		this.sublineas = sublineas;
	}
	/**
	 * @return the sede
	 */
	public SedeCentral getSede() {
		return sede;
	}
	/**
	 * @param sede the sede to set
	 */
	public void setSede(SedeCentral sede) {
		this.sede = sede;
	}
	/**
	 * @return the sublineas
	 */
	public ArrayList<SubLineaAccion> getSublineas() {
		return sublineas;
	}
	/**
	 * @param sublineas the sublineas to set
	 */
	public void setSublineas(ArrayList<SubLineaAccion> sublineas) {
		this.sublineas = sublineas;
	}
	
}
