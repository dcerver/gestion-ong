package gestionOng.logic;
/**
 * @author Software Solutions
 * Trabajadores de las delegaciones: empleado, voluntario o colaborador
 */
public class Trabajador extends Ente {

	private Delegacion delegacion;
	
	/**
	 * @param id
	 * @param nombre
	 * @param delegacion
	 * @param tipo
	 */
	public Trabajador(int id, String nombre, Delegacion delegacion, String tipo) {
		super(id, nombre);
		this.delegacion = delegacion;
		this.tipo = tipo;
	}

	/**
	 * @return the delegacion
	 */
	public Delegacion getDelegacion() {
		return delegacion;
	}

	/**
	 * @param delegacion the delegacion to set
	 */
	public void setDelegacion(Delegacion delegacion) {
		this.delegacion = delegacion;
	}

	private String tipo;

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}
