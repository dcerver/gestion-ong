package gestionOng.logic;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * @author Software Solutions
 * Proyectos que hacen las acciones de la organizacion en diversos paises.
 */
@Entity
public class Proyecto extends Ente {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pais")
	private String pais;
	@Column(name = "localizacion")
	private String localizacion;
	@Column(name = "linea")
	private LineaAccion linea;
	@Column(name = "sublinea")
	private SubLineaAccion sublinea;
	@Column(name = "fechaInicio")
	private Date fechaInicio;
	@Column(name = "fechaFin")
	private Date fechaFin;
	@Column(name = "financiacion")
	private float financiacion;
	@Column(name = "acciones")
	private String acciones;
	private ArrayList<SocioLocal> sociosLocales;
	private ArrayList<Financiador> financiadores;
	private ArrayList<Ente> personal;
	
	/**
	 * @param id
	 * @param nombre
	 * @param pais
	 * @param localizacion
	 * @param linea
	 * @param sublinea
	 * @param fechaInicio
	 * @param fechaFin
	 * @param financiacion
	 * @param acciones
	 * @param sociosLocales
	 * @param financiadores
	 * @param personal
	 */
	public Proyecto(int id, String nombre, String pais, String localizacion, LineaAccion linea, SubLineaAccion sublinea,
			Date fechaInicio, Date fechaFin, float financiacion, String acciones, ArrayList<SocioLocal> sociosLocales,
			ArrayList<Financiador> financiadores, ArrayList<Ente> personal) {
		super(id, nombre);
		this.pais = pais;
		this.localizacion = localizacion;
		this.linea = linea;
		this.sublinea = sublinea;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.financiacion = financiacion;
		this.acciones = acciones;
		this.sociosLocales = sociosLocales;
		this.financiadores = financiadores;
		this.personal = personal;
	}
	public Proyecto() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the pais
	 */
	public String getPais() {
		return pais;
	}
	/**
	 * @param pais the pais to set
	 */
	public void setPais(String pais) {
		this.pais = pais;
	}
	/**
	 * @return the localizacion
	 */
	public String getLocalizacion() {
		return localizacion;
	}
	/**
	 * @param localizacion the localizacion to set
	 */
	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}
	/**
	 * @return the linea
	 */
	public LineaAccion getLinea() {
		return linea;
	}
	/**
	 * @param linea the linea to set
	 */
	public void setLinea(LineaAccion linea) {
		this.linea = linea;
	}
	/**
	 * @return the sublinea
	 */
	public SubLineaAccion getSublinea() {
		return sublinea;
	}
	/**
	 * @param sublinea the sublinea to set
	 */
	public void setSublinea(SubLineaAccion sublinea) {
		this.sublinea = sublinea;
	}
	/**
	 * @return the fechaInicio
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}
	/**
	 * @param fechaInicio the fechaInicio to set
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	/**
	 * @return the fechaFin
	 */
	public Date getFechaFin() {
		return fechaFin;
	}
	/**
	 * @param fechaFin the fechaFin to set
	 */
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	/**
	 * @return the financiacion
	 */
	public float getFinanciacion() {
		return financiacion;
	}
	/**
	 * @param financiacion the financiacion to set
	 */
	public void setFinanciacion(float financiacion) {
		this.financiacion = financiacion;
	}
	/**
	 * @return the acciones
	 */
	public String getAcciones() {
		return acciones;
	}
	/**
	 * @param acciones the acciones to set
	 */
	public void setAcciones(String acciones) {
		this.acciones = acciones;
	}
	/**
	 * @return the sociosLocales
	 */
	public ArrayList<SocioLocal> getSociosLocales() {
		return sociosLocales;
	}
	/**
	 * @param sociosLocales the sociosLocales to set
	 */
	public void setSociosLocales(ArrayList<SocioLocal> sociosLocales) {
		this.sociosLocales = sociosLocales;
	}
	/**
	 * @return the financiadores
	 */
	public ArrayList<Financiador> getFinanciadores() {
		return financiadores;
	}
	/**
	 * @param financiadores the financiadores to set
	 */
	public void setFinanciadores(ArrayList<Financiador> financiadores) {
		this.financiadores = financiadores;
	}
	/**
	 * @return the personal
	 */
	public ArrayList<Ente> getPersonal() {
		return personal;
	}
	/**
	 * @param personal the personal to set
	 */
	public void setPersonal(ArrayList<Ente> personal) {
		this.personal = personal;
	}
	
}
