package gestionOng.logic;

/**
 * @author Software Solutions
 * Delegaciones de la organizacion, distribuidas por el territorio español
 */
public class Delegacion extends Ente {
	private SedeCentral sede;
	private String direccion;
	
	/**
	 * @param id
	 * @param nombre
	 * @param sede
	 * @param direccion
	 */
	public Delegacion(int id, String nombre, SedeCentral sede, String direccion) {
		super(id, nombre);
		this.sede = sede;
		this.direccion = direccion;
	}

	/**
	 * @return the sede
	 */
	public SedeCentral getSede() {
		return sede;
	}
	/**
	 * @param sede the sede to set
	 */
	public void setSede(SedeCentral sede) {
		this.sede = sede;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}
