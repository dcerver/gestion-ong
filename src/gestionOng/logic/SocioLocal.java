package gestionOng.logic;
/**
 * @author Software Solutions
 * Socios Locales que colaboran en algunos proyectos
 */
public class SocioLocal extends Ente {
	private Proyecto proyecto;

	/**
	 * @param id
	 * @param nombre
	 * @param proyecto
	 */
	public SocioLocal(int id, String nombre, Proyecto proyecto) {
		super(id, nombre);
		this.proyecto = proyecto;
	}

	/**
	 * @return the proyecto
	 */
	public Proyecto getProyecto() {
		return proyecto;
	}

	/**
	 * @param proyecto the proyecto to set
	 */
	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}
	
}
