package gestionOng.logic;
import java.util.ArrayList;

/**
 * @author Software Solutions
 * Sublineas de Accion que tienen cada linea, que a su vez contienen los proyectos
 */
public class SubLineaAccion extends Ente {
	private LineaAccion linea;
	private ArrayList<Proyecto> proyectos;
	
	/**
	 * @param id
	 * @param nombre
	 * @param linea
	 * @param proyectos
	 */
	public SubLineaAccion(int id, String nombre, LineaAccion linea, ArrayList<Proyecto> proyectos) {
		super(id, nombre);
		this.linea = linea;
		this.proyectos = proyectos;
	}
	/**
	 * @return the linea
	 */
	public LineaAccion getLinea() {
		return linea;
	}
	/**
	 * @param linea the linea to set
	 */
	public void setLinea(LineaAccion linea) {
		this.linea = linea;
	}
	/**
	 * @return the proyectos
	 */
	public ArrayList<Proyecto> getProyectos() {
		return proyectos;
	}
	/**
	 * @param proyectos the proyectos to set
	 */
	public void setProyectos(ArrayList<Proyecto> proyectos) {
		this.proyectos = proyectos;
	}
	
}
