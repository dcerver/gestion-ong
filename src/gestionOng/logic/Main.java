package gestionOng.logic;

import gestionOng.dao.DaoFactory;
import gestionOng.dao.DelegacionDao;
import gestionOng.dao.SedeCentralDao;
import gestionOng.dao.TrabajadorDao;

/**
 * @author Software Solutions
 * Clase que contiene el metodo main.
 */
public class Main {

	public static void main(String[] args) throws Exception {

		System.out.println("Inicio del Programa:");
		
		DaoFactory.setImplementacion("MySQL");
		DaoFactory.initializeDB();
		
		SedeCentralDao sedeCentralDao = (SedeCentralDao) DaoFactory.getDao("sedeCentral");
		DelegacionDao delegacionDao = (DelegacionDao) DaoFactory.getDao("delegacion");
		TrabajadorDao trabajadorDao = (TrabajadorDao) DaoFactory.getDao("trabajador");
		
		SedeCentral sede = new SedeCentral(1, "Sede ONG", "Gran Via 12, Madrid");
		Delegacion delegacion1 = new Delegacion(1, "Delegacion Marruecos", sede, "Calle Abderahmane Ibn Aouf 12, Marrakech");
		Delegacion delegacion2 = new Delegacion(2, "Delegacion Algeria", sede, "Calle Mokhtari Belhouchat 34, Argel");
		Trabajador trabajador1 = new Trabajador(1, "Carlos Garcia", delegacion1, "Empleado");
		Trabajador trabajador2 = new Trabajador(2, "Marcos Perez", delegacion1, "Voluntario");
		Trabajador trabajador3 = new Trabajador(3, "Alberto Fernandez", delegacion2, "Empleado");
		Trabajador trabajador4 = new Trabajador(4, "Juan Lopez", delegacion2, "Voluntario");
		
		sedeCentralDao.insert(sede);
		delegacionDao.insert(delegacion1);
		delegacionDao.insert(delegacion2);
		trabajadorDao.insert(trabajador1);
		trabajadorDao.insert(trabajador2);
		trabajadorDao.insert(trabajador3);
		trabajadorDao.insert(trabajador4);
		
		System.out.println("Fin del Programa.");
	}

}
