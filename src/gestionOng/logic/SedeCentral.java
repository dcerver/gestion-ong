package gestionOng.logic;

/**
 * @author Software Solutions
 * Sede Central de la organizacion.
 */
public class SedeCentral extends Ente {
	private String direccion;
//	private ArrayList<LineaAccion> lineas;
//	private ArrayList<Cuota> cuotas;

//	/**
//	 * @param id
//	 * @param nombre
//	 * @param direccion
//	 * @param lineas
//	 * @param cuotas
//	 */
//	public SedeCentral(int id, String nombre, String direccion, ArrayList<LineaAccion> lineas,
//			ArrayList<Cuota> cuotas) {
//		super(id, nombre);
//		this.direccion = direccion;
//		this.lineas = lineas;
//		this.cuotas = cuotas;
//	}
	/**
	 * @param id
	 * @param nombre
	 * @param direccion
	 */
	public SedeCentral(int id, String nombre, String direccion) {
		super(id, nombre);
		this.direccion = direccion;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the lineas
	 */
//	public ArrayList<LineaAccion> getLineas() {
//		return lineas;
//	}
	/**
	 * @param lineas the lineas to set
	 */
//	public void setLineas(ArrayList<LineaAccion> lineas) {
//		this.lineas = lineas;
//	}
	/**
	 * @return the cuotas
	 */
//	public ArrayList<Cuota> getCuotas() {
//		return cuotas;
//	}
	/**
	 * @param cuotas the cuotas to set
	 */
//	public void setCuotas(ArrayList<Cuota> cuotas) {
//		this.cuotas = cuotas;
//	}
	
}
