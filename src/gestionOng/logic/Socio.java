package gestionOng.logic;
/**
 * @author Software Solutions
 * Socio que paga una cuota a la organizacion
 */
public class Socio extends Ente {
	private Cuota cuota;

	/**
	 * @param id
	 * @param nombre
	 * @param cuota
	 */
	public Socio(int id, String nombre, Cuota cuota) {
		super(id, nombre);
		this.cuota = cuota;
	}

	/**
	 * @return the cuota
	 */
	public Cuota getCuota() {
		return cuota;
	}

	/**
	 * @param cuota the cuota to set
	 */
	public void setCuota(Cuota cuota) {
		this.cuota = cuota;
	}
	
}
