package gestionOng.dao;

/**
 * @author Sodtware Solutions
 * Dao generico que heredara cada Dao
 */
public interface GenericDao<T> {
    void insert(T t) throws Exception;
    void update(T t) throws Exception;
    void delete(int id) throws Exception;
}
