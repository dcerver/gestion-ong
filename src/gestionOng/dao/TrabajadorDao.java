package gestionOng.dao;

import gestionOng.logic.Trabajador;

/**
 * @author Software Solutions
 * Dao para la clase de Trabajador
 */
public interface TrabajadorDao  extends GenericDao<Trabajador> {

}
