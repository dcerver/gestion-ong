package gestionOng.dao;

import gestionOng.logic.Delegacion;

/**
 * @author Software Solutions
 * Dao para la clase Delegacion
 */
public interface DelegacionDao extends GenericDao<Delegacion> {

}
