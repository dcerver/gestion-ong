package gestionOng.dao;

import gestionOng.logic.SedeCentral;

/**
 * @author Software Solutions
 * Dao para la clase de SedeCentral
 */
public interface SedeCentralDao extends GenericDao<SedeCentral> {

}
