package gestionOng.dao;

import java.sql.SQLException;

import gestionOng.mysql.DelegacionMySQL;
import gestionOng.mysql.MySQLutil;
import gestionOng.mysql.SedeCentralMySQL;
import gestionOng.mysql.TrabajadorMySQL;

/**
 * @author Software Solutions
 * Factoria que se encarga de retornar el tipo de Dao acorde con la implementacion escojida
 */
public class DaoFactory {
	private static String implementacion; // Para escojer implementacion: "MySQL" o "Xml"
	
	/**
	 * @return the implementacion
	 */
	public static String getImplementacion() {
		return implementacion;
	}
	/**
	 * @param implementacion the implementacion to set
	 */
	public static void setImplementacion(String implementacion) {
		DaoFactory.implementacion = implementacion;
	}
	
	public static GenericDao getDao(String tipo){
    if(implementacion.equals("MySQL")) {
        if(tipo.equals("trabajador")){
            return new TrabajadorMySQL();
        }else if(tipo.equals("delegacion")){
        	return new DelegacionMySQL();
        }else if(tipo.equals("sedeCentral")) {
        	return new SedeCentralMySQL();
        }
    }else if(implementacion.equals("Xml")){
    	System.out.println("No implementado.");
    }
	return null;
	}
	public static void initializeDB() throws SQLException {
		if(implementacion.contentEquals("MySQL")) {
			MySQLutil.initializeDB();
		}else if(implementacion.equals("Xml")) {
	    	System.out.println("No implementado.");
		}
	}
}
