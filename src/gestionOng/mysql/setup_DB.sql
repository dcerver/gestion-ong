CREATE DATABASE gestionOngDB;
CREATE USER 'administrador'@'localhost' IDENTIFIED BY 'contraseña';
GRANT ALL PRIVILEGES ON * . * TO 'administrador'@'localhost';
GRANT ALL ON gestionOngDB.* TO 'administrador'@'localhost' IDENTIFIED BY 'contraseña' WITH GRANT OPTION;
FLUSH PRIVILEGES;
