package gestionOng.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author Software Solutions
 * Clase de utilidades para la implementacion de persistencia de datos en MySQL
 */
public class MySQLutil {
	private static String USUARIO = "administrador";
	private static String PASSWORD = "contraseña";
	private static String URL = "jdbc:mysql://localhost/gestionOngDB";
	
	public static Connection getConnection() throws SQLException {
		Connection conn = DriverManager.getConnection(URL, USUARIO, PASSWORD);
		return conn;
	}
	public static void initializeDB() throws SQLException {
		Connection conn = getConnection();
		PreparedStatement stmt;
		String query;
		
		query = "DROP TABLE IF EXISTS trabajadores;";
		stmt = conn.prepareStatement(query);
		stmt.executeUpdate();
		query = "CREATE TABLE trabajadores("
				+ "trabajadorId INTEGER NOT NULL PRIMARY KEY UNIQUE,"
				+ "trabajadorNombre VARCHAR(255) NOT NULL,"
				+ "trabajadorDelegacion VARCHAR(255) NOT NULL,"
				+ "trabajadorTipo VARCHAR(255) NOT NULL"
				+ ");";
		stmt = conn.prepareStatement(query);
		stmt.executeUpdate();
		
		query = "DROP TABLE IF EXISTS delegaciones;";
		stmt = conn.prepareStatement(query);
		stmt.executeUpdate();
		query = "CREATE TABLE delegaciones("
				+ "delegacionId INTEGER NOT NULL PRIMARY KEY UNIQUE,"
				+ "delegacionNombre VARCHAR(255) NOT NULL,"
				+ "delegacionSede INTEGER NOT NULL,"
				+ "delegacionDireccion VARCHAR(255) NOT NULL"
				+ ");";
		stmt = conn.prepareStatement(query);
		stmt.executeUpdate();
		
		query = "DROP TABLE IF EXISTS sedes;";
		stmt = conn.prepareStatement(query);
		stmt.executeUpdate();
		query = "CREATE TABLE sedes("
				+ "sedeId INTEGER NOT NULL PRIMARY KEY UNIQUE,"
				+ "sedeNombre VARCHAR(255) NOT NULL,"
				+ "sedeDireccion VARCHAR(255) NOT NULL"
				+ ");";
		stmt = conn.prepareStatement(query);
		stmt.executeUpdate();
		
		conn.close();
	}
}