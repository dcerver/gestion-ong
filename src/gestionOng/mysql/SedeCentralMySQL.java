package gestionOng.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;

import gestionOng.dao.SedeCentralDao;
import gestionOng.logic.SedeCentral;

/**
 * @author Software Solutions
 * Implementacion de la clase SedeCentral para MySQL
 */
public class SedeCentralMySQL implements SedeCentralDao{
	@Override
	public void insert(SedeCentral t) throws Exception {
		Connection conn = MySQLutil.getConnection();
		PreparedStatement stmt;
		String query;
		query = "INSERT INTO sedes ("
				+ "sedeId, "
				+ "sedeNombre, "
				+ "sedeDireccion"
				+ ") VALUES (?,?,?);";
		stmt = conn.prepareStatement(query);
		stmt.setLong(1, t.getId());
		stmt.setString(2, t.getNombre());
		stmt.setString(3, t.getDireccion());
		stmt.executeUpdate();
		conn.close();
	}

	@Override
	public void update(SedeCentral t) throws Exception {
		Connection conn = MySQLutil.getConnection();
		PreparedStatement stmt;
		String query;
		query = "UPDATE sedes SET "
				+ "sedeNombre= ?, "
				+ "sedeDireccion= ?"
				+ " WHERE sedeId = ?;";
		stmt = conn.prepareStatement(query);
		stmt.setString(1, t.getNombre());
		stmt.setString(2, t.getDireccion());
		stmt.setLong(3, t.getId());
		stmt.executeUpdate();
		conn.close();	
	}

	@Override
	public void delete(int id) throws Exception {
		Connection conn = MySQLutil.getConnection();
		PreparedStatement stmt;
		String query;
		query = "DELETE FROM sedes WHERE sedeId = ?;";
		stmt = conn.prepareStatement(query);
		stmt.setLong(1, id);
		stmt.executeUpdate();
		conn.close();	
	}
}
