package gestionOng.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;

import gestionOng.dao.TrabajadorDao;
import gestionOng.logic.Trabajador;

/**
 * @author Software Solutions
 * Implementacion de la clase Trabajador para MySQL
 */
public class TrabajadorMySQL implements TrabajadorDao{

	@Override
	public void insert(Trabajador t) throws Exception {
		Connection conn = MySQLutil.getConnection();
		PreparedStatement stmt;
		String query;
		query = "INSERT INTO trabajadores ("
				+ "trabajadorId, "
				+ "trabajadorNombre, "
				+ "trabajadorDelegacion, "
				+ "trabajadorTipo"
				+ ") VALUES (?,?,?,?);";
		stmt = conn.prepareStatement(query);
		stmt.setLong(1, t.getId());
		stmt.setString(2, t.getNombre());
		stmt.setInt(3, t.getDelegacion().getId());
		stmt.setString(4, t.getTipo());
		stmt.executeUpdate();
		conn.close();
	}

	@Override
	public void update(Trabajador t) throws Exception {
		Connection conn = MySQLutil.getConnection();
		PreparedStatement stmt;
		String query;
		query = "UPDATE trabajadores SET "
				+ "trabajadorNombre= ?, "
				+ "trabajadorDelegacion= ?, "
				+ "trabajadorTipo= ?"
				+ " WHERE trabajadorId = ?;";
		stmt = conn.prepareStatement(query);
		stmt.setString(1, t.getNombre());
		stmt.setLong(2, t.getDelegacion().getId());
		stmt.setString(3, t.getTipo());
		stmt.setLong(4, t.getId());
		stmt.executeUpdate();
		conn.close();	
	}

	@Override
	public void delete(int id) throws Exception {
		Connection conn = MySQLutil.getConnection();
		PreparedStatement stmt;
		String query;
		query = "DELETE FROM trabajadores WHERE trabajadorId = ?;";
		stmt = conn.prepareStatement(query);
		stmt.setLong(1, id);
		stmt.executeUpdate();
		conn.close();	
	}
	
}
