package gestionOng.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;

import gestionOng.dao.DelegacionDao;
import gestionOng.logic.Delegacion;

/**
 * @author Software Solutions
 * Implementacion de la clase Trabajador para MySQL
 */
public class DelegacionMySQL implements DelegacionDao{

	@Override
	public void insert(Delegacion t) throws Exception {
		Connection conn = MySQLutil.getConnection();
		PreparedStatement stmt;
		String query;
		query = "INSERT INTO delegaciones ("
				+ "delegacionId, "
				+ "delegacionNombre, "
				+ "delegacionSede, "
				+ "delegacionDireccion"
				+ ") VALUES (?,?,?,?);";
		stmt = conn.prepareStatement(query);
		stmt.setLong(1, t.getId());
		stmt.setString(2, t.getNombre());
		stmt.setInt(3, t.getSede().getId());
		stmt.setString(4, t.getDireccion());
		stmt.executeUpdate();
		conn.close();
	}

	@Override
	public void update(Delegacion t) throws Exception {
		Connection conn = MySQLutil.getConnection();
		PreparedStatement stmt;
		String query;
		query = "UPDATE delegaciones SET "
				+ "delegacionNombre= ?, "
				+ "delegacionSede= ?, "
				+ "delegacionDireccion= ?"
				+ " WHERE delegacionId = ?;";
		stmt = conn.prepareStatement(query);
		stmt.setString(1, t.getNombre());
		stmt.setLong(2, t.getSede().getId());
		stmt.setString(3, t.getDireccion());
		stmt.setLong(4, t.getId());
		stmt.executeUpdate();
		conn.close();	
	}

	@Override
	public void delete(int id) throws Exception {
		Connection conn = MySQLutil.getConnection();
		PreparedStatement stmt;
		String query;
		query = "DELETE FROM delegaciones WHERE delegacionId = ?;";
		stmt = conn.prepareStatement(query);
		stmt.setLong(1, id);
		stmt.executeUpdate();
		conn.close();	
	}
}
